const finalhandler = require('finalhandler'),
    fs = require('fs'),
    https = require('https'),
    serveIndex = require('serve-index'),
    serveStatic = require('serve-static'),
    config = require('./config.json'),
    index = serveIndex(config.dir, { 'icons': true, view: 'details' }),
    serve = serveStatic(config.dir),
    options = {
        key: fs.readFileSync(__dirname + '/key.pem'),
        cert: fs.readFileSync(__dirname + '/cert.pem')
    },
    server = https.createServer(options, (req, res) => {
        const header = req.headers['authorization'] || '',
            token = header.split(/\s+/).pop() || '',
            auth = new Buffer(token, 'base64').toString(),
            parts = auth.split(/:/),
            username = parts[0],
            password = parts[1]
        if (auth && config.user === username && config.pass === password) {
            const done = finalhandler(req, res)
            serve(req, res, err => {
                if (err) return done(err)
                index(req, res, done)
            })
        } else {
            res.statusCode = 401
            res.setHeader('WWW-Authenticate', 'Basic realm="' + config.msg + '"')
            res.end('<html><body><h1>' + config.auth + '</h1></body></html>')
        }
    }).listen(config.port, () => {
        console.log(`Listening on ${server.address().address}:${server.address().port}`)
    })
